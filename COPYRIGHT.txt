Login-box, Drupal module

Copyright (C) 2013 Danillo Nunes <loginbox@danillonunes.net>
(http://danillonunes.net).

Login-box is developed with sponsorship from Chuva Inc. (http://chuva-inc.com).

Login-box is distributed under GPL v2 license. See the LICENSE.txt file for
more information, or if this file is missing, see the license text at
(http://www.gnu.org/licenses/gpl-2.0.html).

The "close.png" icon is Copyright of Janis Skarnelis, and distributed under the
terms of FancyBox 1.3.4 GPL license (http://fancybox.net).
